function initialise() {

  // Variables
  var canvas      = document.getElementById('haze-canvas');
  var context     = canvas.getContext('2d');

  var clicked     = false;
  var pointY      = -200;
  var alphaPixels;
  var alphaData;
  var hazeCoverTimer;
  var hazePatchTimer;

  var haze1 = new Image();
  haze1.src = 'img/haze1.png';
  var haze2 = new Image();
  haze2.src = 'img/haze2.png';
  var haze3 = new Image();
  haze3.src = 'img/haze3.png';
  var hazeArray = new Array(haze1, haze2, haze3);

  var time   = 200;
  var height = $(window).height();
  var speed  = height/time;

  var factsArray = new Array();

  var popedUp = false;

  var _mouseX;
  var _mouseY;

  // Document Clicks
  $(document).click(function(e) { 
    
    var mouseX = e.pageX;
    var mouseY = e.pageY;

    $(".under-elem").each(function() {
         var offset = $(this).offset();
         var width  = $(this).width();
         var height = $(this).height();

         if (mouseX > offset.left && mouseX < offset.left+width && mouseY > offset.top && mouseY < offset.top+height){
          if($(this).attr('id') == 'next-btn') {
            TweenMax.to($('.fact'), 0.5, {opacity:0, scale:2.5, onComplete:setupFact, ease:Cubic.easeIn});
          } else {
            window.open($(this).attr('href'), '_blank');
          }
         }
      });

  });

  function supports_canvas() {
    console('supports');
    return !!document.createElement('canvas').getContext;
  }

  // The Set Up

  $('.wipe-text').hide();
  $('#haze-canvas').attr('width' , $(window).width());
  $('#haze-canvas').attr('height' , $(window).height());

  TweenMax.set($('a'), {opacity:0.6});
  
  $('.psi-reading').load('neasg.php');

  context.rect(0, 0, canvas.width, canvas.height);

  $('.popup').hide();

  // The Timers

  function hazeCover()
  {
    
    if(pointY >= $(window).height()) {
      clearInterval(hazeCoverTimer);
      $('.wipe-text').fadeIn(500);
      $('.fact-holder').show();
      initMouseFunctions();
    } else {

      var rX = Math.ceil(Math.random()*canvas.width);
      var rY = pointY;
      //var rS = 0.3+(Math.random()*2 / 2);
      var rS = 0.3;

      context.save();
      context.translate(rX, rY);
      context.rotate(Math.random(2) * Math.PI / 2);
      context.scale(rS, rS);
      context.drawImage(hazeArray[Math.ceil(Math.random()*2)], 0, 0);
      context.restore();

      for (var i = 1; i < 3; i++) {
        rX = Math.ceil(Math.random()*canvas.width);
        rY = pointY - (40*i);
        rS = Math.random()*2 / 2;
        context.save();
        context.translate(rX, rY);
        context.rotate(Math.random(2) * Math.PI / 2);
        context.scale(rS, rS);
        context.drawImage(hazeArray[Math.ceil(Math.random()*2)], 0, 0);
        context.restore();
      };

      if(pointY >= $(window).height()) pointY = $(window).height();
      else pointY+= speed;
    }
  }

  function hazePatch()
  {
    for (var i = 1; i < 5; i++) {
      var rX = Math.ceil(Math.random()*canvas.width);
      var rY = Math.ceil(Math.random()*canvas.height);
      var rS = Math.random()*1 / 2;
      context.save();
      context.translate(rX, rY);
      context.rotate(Math.random(2) * Math.PI / 2);
      context.scale(rS, rS);
      context.drawImage(hazeArray[Math.ceil(Math.random()*2)], 0, 0);
      context.restore();
    }
  }

  $(window).resize(function() {
    $('#haze-canvas').attr('width' , $(window).width());
    $('#haze-canvas').attr('height' , $(window).height());
    $('.wipe-text').hide();
  });


  // The Mouse Events
  
  $('a').mouseover(function() {
    TweenLite.to($(this), 0.3, {opacity:1});
  }).mouseout(function(){
    TweenLite.to($(this), 0.8, {opacity:0.6});
  });

  $('.info').click(function(){
    popedUp = true;
    $('.popup').toggle();
  });

  $('.popup').click(function(){
    popedUp = false;
    $('.popup').hide();
  });

  

  function initMouseFunctions() {
    $(document).mousedown(function(e){
      if(e.which === 1) clicked = true;
      $('.wipe-text').fadeOut(500);      
    });

    $(document).mouseup(function(e){
      if(e.which === 1) clicked = false;
      $('#main-container').css('cursor','default');
    });

    $(document).mousemove(function(e) {
      if(clicked) {
        
        var x = e.pageX - document.getElementById('main-container').offsetLeft;
        var y = e.pageY - document.getElementById('main-container').offsetTop;

        e.preventDefault();

        context.globalCompositeOperation = 'destination-out';

        var grad = context.createRadialGradient(500,150,50,500,150,100);
        grad.addColorStop(0, '#FFFFFF');
        grad.addColorStop(0.5, '#FFFFFF');
        grad.addColorStop(1, '#000000');

        context.beginPath();
        context.arc(x, y, 80, 0, 2 * Math.PI, false);
        context.fillStyle = grad;
        context.fill();

        context.globalCompositeOperation = 'source-over';

      } else {

      }
    });

    //Phone touch

    canvas.addEventListener("touchstart", handleStart, false);
    canvas.addEventListener("touchend", handleEnd, false);
    canvas.addEventListener("touchmove", handleMove, false);
  }


  function handleStart(evt) {
    clicked = true;
    $('.wipe-text').fadeOut(500);
  }

  function handleMove(evt) {
    if(clicked) {
      _mouseX = evt.targetTouches[0].pageX - document.getElementById('main-container').offsetLeft;
      _mouseY = evt.targetTouches[0].pageY - document.getElementById('main-container').offsetTop;
      evt.preventDefault();

      context.globalCompositeOperation = 'destination-out';

      var grad = context.createRadialGradient(500,150,50,500,150,100);
      grad.addColorStop(0, '#FFFFFF');
      grad.addColorStop(0.8, '#FFFFFF');
      grad.addColorStop(1, '#000000');

      context.beginPath();
      context.arc(_mouseX, _mouseY, 40, 0, 2 * Math.PI, false);
      context.fillStyle = grad;
      context.fill();

      context.globalCompositeOperation = 'source-over';
    }
  }

  function handleEnd(evt) {
    clicked = false;

    _mouseX = evt.changedTouches[0].pageX;
    _mouseY = evt.changedTouches[0].pageY;

    $(".under-elem").each(function() {
       var offset = $(this).offset();
       var width  = $(this).width();
       var height = $(this).height();

       if (_mouseX > offset.left && _mouseX < offset.left+width && _mouseY > offset.top && _mouseY < offset.top+height){
        if($(this).attr('id') == 'prev-btn') {
          TweenMax.to($('.fact'), 0.5, {opacity:0, scale:2.5, onComplete:setupFact, ease:Cubic.easeIn});
        } else if($(this).attr('id') == 'next-btn') {
          TweenMax.to($('.fact'), 0.5, {opacity:0, scale:2.5, onComplete:setupFact, ease:Cubic.easeIn});
        } else {
          window.open($(this).attr('href'), '_blank');
        }
       }
    });

  }

  canvas.ontouchstart = function(e) {
    if (e.touches) e = e.touches[0];
    return false;
  }



$.ajax({
  type: "GET",
  url: "haze-facts.xml?v=1",
  dataType: "xml",
  success: function(xml) {
    theXML = xml;
    hazeCoverTimer = setInterval(function(){hazeCover()}, 10);
    hazePatchTimer = setInterval(function(){hazePatch()}, 750);
    setupFact();
  }
});

var factNum = 0;

function setupFact() {

  var r = factNum;
  var fact = $(theXML).find('fact').eq(r);
  var q = "Fact #"+($(theXML).find('fact').length-r)+": "+fact.find('question').text();
  var a = fact.find('answer').text();

  $('.question').html(q);
  $('.answer').html(a);
  $('.sources').html('');

  var s = '';
  if (fact.find('source').length > 1) s = 1;

  fact.find('source').each(function() {
   $('.sources').append('<li><div class="under-elem source" href="' + $(this).text() + '" target="_blank">Source ' + s + '</div></li>');
   s++;
  });

  TweenMax.set($('.fact'), {opacity:0, scale:0.6});
  TweenMax.to($('.fact'), 0.5, {opacity:1, scale:1, ease:Cubic.easeOut});

  if(factNum >= $(theXML).find('fact').length-1){
    factNum = Math.floor(Math.random()*($(theXML).find('fact').length));
  } else {
    factNum++;  
  }
}






}


function console(_msg){
  console.log(_msg);
}

window.onload = function() {

  $('body').css('display','block');
  initialise();
  
  // if(navigator.userAgent.match(/iPhone/i)) {
  //   setTimeout(function(){
  //     // Hide the address bar!
  //     $('body').height(480);
  //     window.scrollTo(0, 1);
  //   }, 100);
  // }
}